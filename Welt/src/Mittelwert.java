import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {
	   
	  Scanner scanner = new Scanner(System.in);

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
	  System.out.print("Geben Sie eine Zahl ein:");
      double x = scanner.nextDouble();
      System.out.print("Geben Sie eine Zahl ein:");
      double y = scanner.nextDouble();
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = mittelwert(x + y);
      ausgabe(x,y,m);
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben: sy
      // =================================
      // System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   public static double mittelwert(double x, double y) {
	   return (x + y) / 2.0;
   }
   
   
   public static void ausgabe(double x, double y, double m) {
	   System.out.printf(" Der Mittelwert von &.2f und %.2f ist %.2f",x ,y ,m );
   }
}
